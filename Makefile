# Makefile for creating the PiDP-11 software
#
#

# Your C compiler
#CC = aarch64-linux-gnu-gcc-10		# 64-bit Raspberry Pi
#CC = arm-linux-gnueabihf-gcc-10	# 32-bit Raspberry Pi
CC = gcc

# The address and data knobs should cause their LEDs to progress
# clockwise when turned clockwise and anticlockwise when turned
# anticlockwise.  Sometimes the polarity of the encoders are reversed
# from what is expected.  If this happens to you, uncomment the
# following and recompile.
#
#ENCODERS_REVERSED = yes

RANLIB ?= $(shell which ranlib)
AR ?= $(shell which ar

MAKE_TARGET_ARCH = RPI
MAKE_CONFIGURATION = DEBUG
#MAKE_CONFIGURATION = RELEASE

NAME = pidp11
VERSION = 0.1

PREFIX ?= /opt/pidp11
#PREFIX ?= /usr/localshare/pidp11
BINPATH ?= $(PREFIX)/bin

ETCDIR ?= /etc
RCDIR  ?= $(ETCDIR)/init.d
RCFILE ?= rc.pidp11
CONFIGFILE ?= pidp11.conf

SRCDIR = src
GIT_DIR ?= .git
BLINKENLIGHTS_DIR = $(SRCDIR)/11_pidp_server/pidp11
SCANSWITCH_DIR = $(SRCDIR)/11_pidp_server/scanswitch
SIMH_DIR = $(SRCDIR)/02.3_simh/4.x+realcons/src

SUBDIRS = $(BLINKENLIGHTS_DIR) $(SCANSWITCH_DIR) $(SIMH_DIR)
SUB_CLEAN = $(SUBDIRS:%=%-clean)


export MAKE_TARGET_ARCH
export MAKE_CONFIGURATION
export MAKETARGETS
export CC
export PREFIX
export BINPATH
export SHARE
export RANLIB
export AR
export ENCODERS_REVERSED

all:	pidp1170_blinkenlightd scanswitch simh

lights: pidp1170_blinkenlightd
pidp1170_blinkenlightd:
	$(MAKE) -C $(BLINKENLIGHTS_DIR)

scanswitch:
	$(MAKE) -C $(SCANSWITCH_DIR)

simh:	pidp1170_blinkenlightd
	$(MAKE) -C $(SIMH_DIR)

install: all
	# Set up pidp11 init script.
	install -m 755 etc/$(RCFILE) $(RCDIR)/$(RCFILE)
	install -m 755 etc/pdp.sh  /etc

	install -m 644 etc/$(CONFIGFILE) $(ETCDIR)/$(CONFIGFILE)
	sed -i s,^PREFIX.*,PREFIX=$(PREFIX),g $(ETCDIR)/$(CONFIGFILE)
	update-rc.d pidp11 defaults

	install -d $(BINPATH) -m 755
	# Installing scripts
	install -m 755 bin/pidp11.sh $(BINPATH)
	install -m 755 bin/down.sh $(BINPATH)
	install -m 755 bin/getsel.sh $(BINPATH)
	install -m 755 bin/rebootsimh.sh $(BINPATH)
	install -m 755 bin/test.py $(BINPATH)
	# Installing executable binaries
	install -m 755 bin/client11 $(BINPATH)
	install -m 755 bin/scansw $(BINPATH)
	install -m 755 bin/server11 $(BINPATH)

uninstall:
	# Uninstalling scripts
	rm -f $(BINPATH)/pidp11.sh
	rm -f $(BINPATH)/down.sh
	rm -f $(BINPATH)/getsel.sh
	rm -f $(BINPATH)/rebootsimh.sh
	rm -f $(BINPATH)/test.py
	# Uninstalling executable binaries
	rm -f $(BINPATH)/client11
	rm -f $(BINPATH)/scansw
	rm -f $(BINPATH)/server11
	# Uninstalling pidp11 config file
	rm -f $(ETCDIR)/$(CONFIGFILE)
	# Uninstalling pidp11 init script
	rm -f $(RCDIR)/$(RCFILE)
	update-rc.d pidp11 defaults
	rm -f /etc/pdp.sh

$(SUB_CLEAN):
	-$(MAKE) -C $(@:%-clean=%) clean

clean:  $(SUB_CLEAN)

distclean: clean
	rm -f $(NAME)-$(VERSION).tar.gz

dist:
ifneq ($(and $(wildcard $(GIT_DIR)),$(shell which git)),)
	git archive --format=tgz --prefix $(NAME)-$(VERSION)/ HEAD -o $(NAME)-$(VERSION).tar.gz
	@echo $(NAME)-$(VERSION).tar.gz created.
else
	@echo "Not in a git repository or git command not found.  Cannot make a tarball."
endif

.PHONY: all clean distclean dist install uninstall
